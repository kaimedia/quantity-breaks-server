'use strict';

const express = require('express');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');
const moment = require('moment-timezone');
const logger = require('./lib/logger');
const config = require('./config');
const mongoose = require('mongoose');
const hbs = require('hbs');
require('dotenv').config();

const app = express();
const server = http.createServer(app);

app.set('port', process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());
app.use(helmet());
app.use(cookieParser());
app.use(cors());
app.disable('etag');
app.set('trust proxy', 1);
app.use(morgan('dev', {
    skip: (req, res) => res.statusCode < 400
}));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'js');
app.engine('js', hbs.__express);

app.use('/', require('./routes/index'));
app.use('/install', require('./routes/install'));
app.use('/api/batches', require('./routes/batche'));
app.use('/api/products', require('./routes/product'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/user', require('./routes/user'));
app.use('/api/shop', require('./routes/shop'));
app.use('/api/file', require('./routes/file'));
app.use('/assets', [require('./routes/asset'), express.static(path.join(__dirname, 'public'))]);
app.use('/webhook', require('./routes/webhook'));

app.use((err, req, res, next) => {
    if (!err) return next();
    res.status(res.statusCode).send({
        message: err.message,
        error: err
    });
});

if (process.env.NODE_ENV === "production") {
    console.log = () => { };
}

mongoose.connect(config.MONGODB_URI, {
    useMongoClient: true,
});
mongoose.Promise = require('bluebird');
mongoose.connection.on('error', (err) => logger.error(`Database Error  → ${err}`));
mongoose.connection.once('open', () => logger.info('MongoDB is connected!'));

server.listen(app.get('port'), () => {
    logger.info('[SERVER] %s', moment().tz(config.TIMEZONE).format('YYYY-MM-DD HH:mm:ss Z'));
    logger.info(`[SERVER] Listening on port: ${app.get('port')}`);
});

server.timeout = 240000;

module.exports = server;