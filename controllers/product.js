'use strict';

const _ = require('lodash');
const logger = require('../lib/logger');
const config = require('../config');
const Shopify = require('shopify-node-api');
const Shop = require('../models/shop');
const Group = require('../models/group');
const Promise = require('bluebird');

function list(req, res, next) {
    const query = Shop.findOne({ shopify_domain: req.params.shop }).exec();
    query.then((result) => {
        const shop = result;
        const shopAPI = new Shopify({
            shop: req.params.shop,
            shopify_api_key: config.SHOPIFY_API_KEY,
            shopify_shared_secret: config.SHOPIFY_SHARED_SECRET,
            access_token: shop.accessToken,
        });
        shopAPI.get('/admin/products.json', { limit: 250, published_status: 'published', fields: 'id,title,handle,image,variants,vendor,product_type,template_suffix' }, (err, results) => {
            getProductIds(req.params.shop).then((productIds) => {
                // const products = results['products'].filter((e) => e.template_suffix == 'break');
                const products = results['products'];
                products.forEach(product => {
                    if (_.includes(productIds, product.id)) {
                        product.disabled = true;
                    } else {
                        product.disabled = false;
                    }
                    return product;
                });
                res.status(200).send(products);
            }, err => next(err));
        });
    });
}

function getProductIds(shopDomain) {
    return new Promise((resolve, reject) => {
        let productIds = [];
        Group.find({ shop: shopDomain }).exec().then((data) => {
            if (data) {
                data.forEach(bundle => {
                    if (bundle.products) {
                        bundle.products.forEach(element => {
                            productIds.push(element.id);
                        });
                    }
                });
                resolve(productIds);
            }
        });
    });
}

exports.list = list;
