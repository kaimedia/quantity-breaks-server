'use strict';

const logger = require('../lib/logger');
const User = require('../models/user');
const secret = require('../config').SECRET;
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const expireToken = "8h";

function login(req, res) {
    User.findOne({ email: req.body.email, active: true }, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'Error on the server.' });

        if (!user)
            return res.status(404).send({ message: 'No user found.' });

        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);

        if (!passwordIsValid)
            return res.status(401).send({ token: null, message: "Login Failed. Password is incorrect" });

        var token = jwt.sign({ id: user._id }, secret, {
            expiresIn: expireToken
        });

        res.status(200).send({ success: true, token: token });
    });
}

function logout(req, res) {
    res.status(200).send({ success: false, token: null });
}

function register(req, res) {
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    User.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPassword,
        role: 'guest'
    }, (err, user) => {
        if (err)
            return res.status(500).send({ message: 'There was a problem creating the user: ' + err.message })

        var token = jwt.sign({ id: user._id }, secret, {
            expiresIn: expireToken
        });

        res.status(200).json({ success: true, token: token });
    });
}

function me(req, res, next) {
    User.findOne({ _id: req.userId, active: true }, { password: 0 }, (err, user) => {
        if (err)
            return res.status(500).send({ message: "There was a problem finding the user." });

        if (!user)
            return res.status(404).send({ message: "No user found." });

        res.status(200).send(user);
    });
}

exports.me = me;
exports.login = login;
exports.logout = logout;
exports.register = register;