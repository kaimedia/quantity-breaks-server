'use strict';

const logger = require('../lib/logger');
const bcrypt = require('bcryptjs');
const User = require('../models/user');
const node_acl = require('acl');
const acl = new node_acl(new node_acl.memoryBackend());

function create(req, res, next) {
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    User.create({
        email: req.body.email,
        password: hashedPassword,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        address: req.body.address,
        role: req.body.role
    }, (err, user) => {
        if (err) return next(err);
        acl.addUserRoles(String(user._id), String(user.role));
        res.status(200).send(user);
    });
}

function all(req, res, next) {
    User.find({}, (err, users) => {
        if (err) return next(err);
        res.status(200).send(users);
    });
}

function get(req, res, next) {
    User.findOne({ _id: req.params.id, active: true }, (err, user) => {
        if (err) return next(err);
        if (!user)
            return res.status(404).send({ message: "No user found." });
        res.status(200).send(user);
    });
}

function remove(req, res, next) {
    User.findByIdAndRemove(req.params.id, (err, user) => {
        if (err) return next(err);
        acl.removeUserRoles(String(user._id), String(user.role));
        res.status(200).send({ message: "User: " + user.email + " was deleted." });
    });
}

function update(req, res, next) {
    User.findByIdAndUpdate(req.body._id, req.body, { new: true }, (err, user) => {
        if (err) return next(err);
        res.status(200).send({ message: "Updated user successfully" });
    });
}

function password(req, res, next) {
    const newPassword = bcrypt.hashSync(req.body.newPassword, 8);

    User.findByIdAndUpdate(req.body._id, { password: newPassword }, { new: true }, (err, user) => {
        if (err) return next(err);
        res.status(200).send({ message: "Change password successfully" });
    });
}

function changeStatus(req, res, next) {
    const id = req.params.id;
    User.findByIdAndUpdate(id, req.body, (err, results) => {
        if (err) return next(err);
        User.findById(id, (err, user) => {
            if (err) return next(err);
            res.status(200).json(user);
        });
    });
}

exports.create = create;
exports.all = all;
exports.get = get;
exports.remove = remove;
exports.update = update;
exports.password = password;
exports.changeStatus = changeStatus;