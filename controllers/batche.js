'use strict';

const _ = require('lodash');
const AWS = require('aws-sdk');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Group = require('../models/group');
const Shopify = require('shopify-node-api');
const batcheService = require('../services/batche');
const batcheService2 = require('../services/batche2');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(config.SENDGRID.apiKey);
const moment = require('moment-timezone');
moment.tz.setDefault(config.TIMEZONE);

const S3 = new AWS.S3({
    accessKeyId: config.AWS_ACCESS_KEY,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
});

function get(req, res, next) {
    Group.findOne({ shop: req.params.shop, _id: req.params.id }).exec()
        .then((group) => res.status(200).send(group));
}

function list(req, res, next) {
    Group.find({ shop: req.params.shop }).exec()
        .then((groups) => res.status(200).send(groups));
}

function create(req, res, next) {
    const products = req.body.products;
    new Group({
        shop: req.body.shop,
        name: req.body.name,
        price_type: req.body.price_type,
        across_variants: req.body.across_variants,
        price_levels: req.body.price_levels,
        products: products,
        productIds: indexProductIds(products),
        status: 1,
    }).save((err, group) => {
        if (err) return next(err);
        batcheService.updateVariants(req.body.shop, products, req.body.price_levels).then(data => {
            batcheService.saveCustomVariants(group, data).then((response) => {
                saveJsonData(req.body.shop);
                res.status(200).send(response);
            }, err => next(err));
        }, err => next(err));
    });
}

function update(req, res, next) {
    const products = req.body.products;

    if (req.body.status == 0) {
        _.map(products, product => {
            product.qbVariants = [];
            return product;
        });
    }

    Group.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        price_type: req.body.price_type,
        across_variants: req.body.across_variants,
        price_levels: req.body.price_levels,
        products: products,
        productIds: indexProductIds(products),
        status: req.body.status,
    }, { new: true }, (err, group) => {
        if (err) return next(err);
        if (req.body.status === 1) {
            batcheService.updateVariants(req.body.shop, products, req.body.price_levels).then(data => {
                batcheService.saveCustomVariants(group, data).then((response) => {
                    saveJsonData(req.body.shop);
                    res.status(200).send(response);
                }, err => next(err));
            }, err => next(err));
        } else {
            saveJsonData(req.body.shop);
            batcheService.removeVariants(req.body.shop, products).then(response => {
                res.status(200).send(response);
            }, err => next(err));
        }
    });
}

function updateAll(req, res, next) {
    const messageSuccess = 'Hello Admin! Your products was updated successfully!';

    let emailMsg = {
        to: 'dinhgiang2611@gmail.com',
        from: config.SENDGRID.from,
        subject: 'UPDATED PRODUCT SUCCESSFULLY | QUANTITY BREAKS',
        text: messageSuccess,
    };

    sgMail.send({
        to: emailMsg.to,
        from: emailMsg.from,
        subject: 'UPDATING ALL PRODUCTS | QUANTITY BREAKS',
        text: 'Hello Admin! Your products are updating the Variants.',
    });

    if (req.body.groups) {
        batcheService2.run(req.params.shop, groups, req.body.price_levels).then(() => {
            saveJsonData(req.params.shop).then((response) => {
                logger.info(messageSuccess);
                res.status(200).send(response);
                sgMail.send(emailMsg);
            });
        }, err => next(err));
    } else {
        Group.find({ shop: req.params.shop, status: 1 }, (err, groups) => {
            if (err) return next(err);
            batcheService2.run(req.params.shop, groups, req.body.price_levels).then(() => {
                saveJsonData(req.params.shop).then((response) => {
                    logger.info(messageSuccess);
                    res.status(200).send(response);
                    sgMail.send(emailMsg);
                });
            }, err => next(err));
        });
    }
}

function remove(req, res, next) {
    Group.findById(req.params.id).exec().then((group) => {
        const products = group.products;
        Group.findByIdAndRemove(req.params.id).exec().then((group) => {
            batcheService.removeVariants(req.params.shop, products).then(response => {
                res.status(200).send(response);
            }, err => next(err));
        });
    });
}

function indexProductIds(products) {
    let productArr = [];
    products.forEach(el => {
        productArr.push(el.id);
    });
    return productArr.join("|");
}

function reMakeProducts(data) {
    data.products = _.map(data.products, product => {
        product.variants = _.map(product.variants, variant => {
            let _variant = _.pick(variant, ['id', 'product_id', 'title', 'price', 'sku', 'inventory_policy', 'fulfillment_service', 'inventory_management', 'option1', 'option2', 'option3', 'taxable', 'image_id', 'inventory_quantity', 'weight', 'weight_unit', 'old_inventory_quantity', 'requires_shipping']);
            return _variant;
        });
        return product;
    });
    return data.products;
}

function saveJsonData(shop, empty = false) {
    return new Promise((resolve, reject) => {
        const query = Group.find({ shop: shop, status: 1 }).exec();
        query.then((response) => {
            let body = response;
            if (empty) {
                body = [];
            }
            S3.upload({
                Bucket: `${config.S3_BUCKET}`,
                Key: `${config.S3_FOLDER}/groups/${shop}.json`,
                Body: Buffer.from(JSON.stringify(body)),
                ContentType: 'application/json',
                ACL: 'public-read',
            }, (err, data) => {
                if (err) return reject({ message: 'Save JSON error!' });
                resolve(data);
            });
        });
    });
}

exports.get = get;
exports.list = list;
exports.create = create;
exports.update = update;
exports.remove = remove;
exports.saveJsonData = saveJsonData;
exports.updateAll = updateAll;