'use strict';

const AWS = require('aws-sdk');
const config = require('../config');
const slugify = require('slugify');
const moment = require('moment-timezone');
moment.tz.setDefault(config.TIMEZONE);

const S3 = new AWS.S3({
    accessKeyId: config.AWS_ACCESS_KEY,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
});

function upload(req, res, next) {
    const data = req.body;
    const shop = req.params.shop;
    const timestamp = moment().unix();
    const fileName = slugify(`${timestamp}_${data.filename}`, { replacement: '-', remove: null, lower: true });
    const imageUrl = `${config.S3_URI}/images/${fileName}`;
    const body = new Buffer(data.value.replace(/^data:image\/\w+;base64,/, ""), 'base64')
    S3.upload({
        Bucket: `${config.S3_BUCKET}`,
        Key: `${config.S3_FOLDER}/images/${fileName}`,
        Body: body,
        ContentEncoding: 'base64',
        ContentType: data.filetype,
        ACL: 'public-read',
    }, (err, body) => {
        if (err) return next(err);
        res.status(200).send({ url: imageUrl });
    });
}

exports.upload = upload;