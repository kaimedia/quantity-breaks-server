function qbInit() {
    qbProductId = qbGroupInfo = qbProductInfo = qbProductVariant = null;
    qbShopDomain = "{{shopDomain}}";
    qbServerUrl = "{{apiURI}}";
    qbJsonUrl = "{{groupData}}?v=" + (new Date().getTime());
    qbStyleUrl = "{{styleURI}}?v=" + (new Date().getTime());

    if (window.ShopifyAnalytics) {
        if (window.ShopifyAnalytics.meta && window.ShopifyAnalytics.meta.page) {
            if (window.ShopifyAnalytics.meta.page.pageType === 'product') {
                qbProductId = window.ShopifyAnalytics.meta.page.resourceId;
            }
        }
    }

    $('head').append('<link rel="stylesheet" type="text/css" href="' + qbStyleUrl + '" />');
}

(function () {
    qbInit();

    var template = qbGetTemplate();

    if (template === "cart") {
        qbSwitchVariant();
    }

    if (template === "product") {
        qbGenerateTable();

        $("#AddToCart, #AddToCart--product-template").click(function () {
            var addCartBtn = $(this);
            var addToCartForm = $("#AddToCartForm--product-template");
            addCartBtn.attr('disabled', true);
            qbUpdateVariantCart(function (err, res) {
                addCartBtn.attr('disabled', false);
                if (err || !res) {
                    addToCartForm.submit();
                } else {
                    if (err || !res || res === 'submit') {
                        addToCartForm.submit();
                    } else {
                        if (!res.cartUpdate || Object.keys(res.cartUpdate).length === 0) {
                            addToCartForm.submit();
                        } else {
                            var jqxhr = $.post('/cart/update.js', { updates: res.cartUpdate });
                            jqxhr.always(function (res) {
                                window.location.href = '/cart';
                            });
                        }
                    }
                }
            });
        });

        $(".js-quantity-selector").change(function () {
            qbProductSelect();
        });
    }
})(window);

function qbProductSelect() {
    if (!qbGroupInfo) {
        return;
    }

    qbGetGroup(qbGroupInfo, function (res) {
        if (!res) {
            return;
        }

        qbShowTable(qbGetTableValues(res.thisGroup, res.thisVariant));

        var qty = $(".js-quantity-selector").val();
        var thisItem = {
            variant_id: qbProductVariant.id,
            product_id: qbProductVariant.product_id,
            line_quantity: qty,
        };

        qbCheckCustomVariant(qbProductInfo, thisItem, function (res) {
            if (!res.newVariant) {
                $("#ProductSelect option").each(function () {
                    $(this).removeAttr('selected');
                });
                return;
            }
            var newVariantId = res.newVariant.custom_variant_id;
            $("#ProductSelect option").each(function () {
                if ($(this).val() == newVariantId) {
                    $(this).attr('selected', 'selected');
                } else {
                    $(this).removeAttr('selected');
                }
            });
        });
    });
}

function qbUpdateVariantCart(callback) {
    var _productGroups = cartItems = jsonCart = [];
    var _variantInput = thisItem = {};
    var qty = $(".js-quantity-selector").val();

    if (!qbProductInfo) { // khong co trong group
        return callback(true, null);
    }

    var jqxhr = $.get('/cart.js');
    jqxhr.always(function (res) {
        if (res && res.status == 200) {
            jsonCart = JSON.parse(res.responseText);
            if (Object.keys(jsonCart.items).length > 0) {
                jsonCart.items.forEach(function (e) {
                    var product_id = e.product_id;
                    var variant_id = e.variant_id;
                    var line_quantity = e.quantity;
                    var title = e.variant_title ? e.variant_title : e.variant_options[0];
                    var price = e.price;
                    _productGroups.push({ product_id: product_id, variant_id: variant_id, price: price, line_quantity: line_quantity, title: title });
                });
                cartItems = _productGroups;
            }

            _variantInput = { productId: qbProductId, qty };

            if ($(".qbSingleOptionSelect").length === 1) {
                var _variantTitle = $(".qbSingleOptionSelect").find(':selected').val();
                thisItem = cartItems.find(function (e) {
                    return e.product_id === qbProductId && e.title.indexOf(_variantTitle) > -1;
                });
            } else {
                thisItem = cartItems;
                $(".qbSingleOptionSelect").each(function (i, ele) {
                    var _variantTitle = $(ele).find(':selected').val();
                    thisItem = thisItem.filter(function (e) {
                        return e.product_id === qbProductId && e.title.indexOf(_variantTitle) > -1;
                    });
                });
                thisItem = thisItem[0];
            }

            if (!thisItem) {
                // khong co trong cart
                thisItem = {
                    variant_id: qbProductVariant.id,
                    product_id: _variantInput.productId,
                    line_quantity: parseInt(_variantInput.qty),
                };
            } else {
                // co trong cart
                thisItem.line_quantity = thisItem.line_quantity + parseInt(_variantInput.qty);
            }

            qbCheckCustomVariant(qbProductInfo, thisItem, function (response) {
                var _oldVariants = _newVariants = _cartVariant = {};
                var newVariant = response.newVariant;
                var variantString = response.groupVariantStr;

                if (!newVariant) {
                    return callback(true, null);
                }

                cartItems.forEach(function (e) {
                    if (e) {
                        var quantity = e.line_quantity;
                        if (variantString.indexOf(e.variant_id) > -1) {
                            quantity = 0;
                        }
                        _oldVariants[e.variant_id] = quantity;
                    }
                });

                _newVariants[newVariant.custom_variant_id] = newVariant.new_qty;

                if (_oldVariants[newVariant.origin_variant_id]) {
                    _oldVariants[newVariant.origin_variant_id] = 0;
                }

                _cartVariant = Object.assign(_oldVariants, _newVariants);
                callback(false, { cartUpdate: _cartVariant });
            });
        }
    });
}

function qbCheckCustomVariant(thisProduct, thisItem, callback) {
    var productID = thisItem.product_id;
    var variantID = thisItem.variant_id;
    var variantQty = thisItem.line_quantity;
    var isReset = false;
    var newVariant = null;
    var currentVariants = [];
    var groupVariantStr = [];
    var thisVariant = {};

    currentVariants = thisProduct.qbVariants.filter(function (e) {
        return e.origin_variant_id === variantID;
    });

    if (Object.keys(currentVariants).length === 0) {
        isReset = true;
        thisVariant = thisProduct.qbVariants.find(function (e) {
            return e.custom_variant_id === variantID;
        });
        currentVariants = thisProduct.qbVariants.filter(function (e) {
            return e.origin_variant_id === thisVariant.origin_variant_id;
        });
    }

    currentVariants = currentVariants.sort(function (a, b) {
        return b.qty - a.qty;
    });

    currentVariants.forEach(function (e) {
        groupVariantStr.push(e.custom_variant_id);
    });

    groupVariantStr = groupVariantStr.join(",");

    var countVariant = Object.keys(currentVariants).length;
    for (var i = 0; i < countVariant; i++) {

        if (variantQty >= currentVariants[i].qty) {
            currentVariants[i].new_qty = variantQty;
            newVariant = currentVariants[i];
            break;
        }

        if (isReset && variantQty <= currentVariants[i].qty) {
            newVariant = {
                origin_variant_id: currentVariants[i].custom_variant_id,
                custom_variant_id: currentVariants[i].origin_variant_id,
                qty: currentVariants[i].qty,
                new_qty: variantQty,
            };
        }

    }

    callback({ newVariant, groupVariantStr });
}

function qbOnChangeVariant() {
    if (qbGroupInfo) {
        qbGetGroup(qbGroupInfo, function (res) {
            if (!res) {
                return;
            }
            var tableValues = qbGetTableValues(res.thisGroup, res.thisVariant);
            qbShowTable(tableValues);
            // qbShowSelectOption(tableValues);
        });
    }
}

function qbSwitchVariant(timeout = 0) {
    qbButtonStatus(true);
    setTimeout(function () {
        qbGenerateVariant(function (response) {
            var cartUpdate = response.cartUpdate;
            var customVariants = response.customVariants;
            var reloadPage = response.reloadPage;
            var linePrices = response.linePrices;

            if (linePrices && Object.keys(linePrices).length > 0) {
                qbSavingCartItems(linePrices);
            }

            if (!reloadPage || !cartUpdate || Object.keys(cartUpdate).length === 0) {
                qbButtonStatus(false);
                return;
            }

            qbUpdateCart(cartUpdate);
        });
    }, timeout);
}

function qbUpdateCart(data) {
    var jqxhr = $.post('/cart/update.js', { updates: data });
    jqxhr.always(function (res) {
        qbButtonStatus(false);
        if (res && res.status == 200) {
            var jsonCart = JSON.parse(res.responseText);
            if (Object.keys(jsonCart.items).length === 0) {
                return;
            }
            window.location.reload();
        };
    });
}

function qbGenerateVariant(callback) {
    var _productGroups = [];
    var jqxhr = $.get('/cart.js');
    jqxhr.always(function (res) {
        if (res && res.status == 200) {
            var jsonCart = JSON.parse(res.responseText);
            if (Object.keys(jsonCart.items).length === 0) {
                qbButtonStatus(false);
                return;
            }
            jsonCart.items.forEach(function (element) {
                _productGroups.push({ product_id: element.product_id, variant_id: element.variant_id, line_quantity: element.quantity });
            });
            qbMakeRequest("POST", qbServerUrl + "/assets/generate_variant/" + qbShopDomain, { products: _productGroups }, function (response) {
                var data = response;
                try {
                    data = JSON.parse(response);
                    callback(data);
                }
                catch (err) {
                    qbButtonStatus(false);
                    return;
                }
            });
        } else {
            qbButtonStatus(false);
            return;
        }
    });
}

function qbSavingCartItems(variants) {
    variants.forEach(function (element) {
        var itemLinePrice = element.origin_price;
        var itemDiscountPrice = element.price;
        if ($(".cart__row__" + element.variant_id).length) {
            $(".cart__row__" + element.variant_id).find(".cart__price__item").html('<span class="qb-cart-item-line-price qb-cart-item__price--old money" style="text-decoration:line-through;opacity:.75">$' + itemLinePrice + ' USD</span> <br> <span class="qb-cart-item-discount-price money" style="color:#900">$' + itemDiscountPrice + ' USD</span>');
        }
    });
}

function qbGetGroup(groups, callback) {
    if (!groups || Object.keys(groups).length === 0) {
        return callback(null);
    }

    var thisGroup = thisProduct = thisVariant = currentVariantTitle = null;

    thisGroup = groups.find(function (e) {
        return e.productIds.indexOf(qbProductId) != -1;
    });

    if (!thisGroup || typeof thisGroup !== 'object' || Object.keys(thisGroup.price_levels).length === 0 || Object.keys(thisGroup.products).length === 0) {
        return callback(null);
    }

    thisProduct = thisGroup.products.find(function (e) {
        return e.id == qbProductId;
    });

    qbProductInfo = thisProduct;

    if ($(".qbSingleOptionSelect").length === 1) {
        currentVariantTitle = $(".qbSingleOptionSelect").find(':selected').val();
        thisVariant = thisProduct.variants.find(function (e) {
            return e.title == currentVariantTitle;
        });
    } else {
        thisVariant = thisProduct.variants;
        $(".qbSingleOptionSelect").each(function (i, ele) {
            var variantName = $(ele).find(':selected').val();
            thisVariant = thisVariant.filter(function (e) {
                return e.title.indexOf(variantName) > -1;
            });
        });
        thisVariant = thisVariant[0];
    }

    qbProductVariant = thisVariant;

    if (!thisVariant) {
        return callback(null);
    }

    callback({ thisGroup, thisVariant });
}

function qbGenerateTable() {
    if (!qbProductId) {
        return;
    }

    if (qbGroupInfo) {
        qbGetGroup(qbGroupInfo, function (res) {
            if (!res) {
                return;
            }
            var tableValues = qbGetTableValues(res.thisGroup, res.thisVariant);
            qbShowTable(tableValues);
            // qbShowSelectOption(tableValues);
        });
    } else {
        var addCartBtn = $("#AddToCart, #AddToCart--product-template");
        addCartBtn.attr('disabled', true);
        qbMakeRequest("GET", qbJsonUrl, function (response) {
            addCartBtn.attr('disabled', false);
            var groups = response;
            try {
                groups = JSON.parse(response);
                qbGroupInfo = groups;
                qbGetGroup(qbGroupInfo, function (res) {
                    if (!res) {
                        return;
                    }
                    var tableValues = qbGetTableValues(res.thisGroup, res.thisVariant);
                    qbShowTable(tableValues);
                    // qbShowSelectOption(tableValues);
                });
            }
            catch (err) {
                return console.log(err);
            }
        });
    }
}

function qbGetTableValues(thisGroup, thisVariant) {
    var tableValues = [];

    var priceLevelLength = Object.keys(thisGroup.price_levels).length;
    for (var i = 0; i < priceLevelLength; i++) {
        var level = thisGroup.price_levels[i];
        var _price = _percent = 0;
        if (thisGroup.price_type === "%") {
            _price = qbMixPrice((Number(thisVariant.price) - ((Number(thisVariant.price) * Number(level.value)) / 100)).toFixed(2));
            _price = "$" + _price + " USD";
            _percent = level.value + "% OFF";
        }
        tableValues.push({
            qty: level.qty,
            percent: _percent,
            price: _price,
        });
    }

    return tableValues;
}

function qbShowTable(data) {
    var htmlTable = "";

    htmlTable += "<table id='quantity-breaks-table-grid'>";
    htmlTable += "<thead>";
    htmlTable += "<tr>";
    htmlTable += "<th>Qty</th>";
    htmlTable += "<th>Price</th>";
    htmlTable += "<th>Discount</th>";
    htmlTable += "</tr>";
    htmlTable += "</thead>";
    htmlTable += "<tbody>";

    data.forEach(function (e) {
        htmlTable += "<tr>";
        htmlTable += "<td>" + e.qty + "+</td>";
        htmlTable += "<td class='_price'>" + e.price + "</td>";
        htmlTable += "<td class='_percent'>" + e.percent + "</td>";
        htmlTable += "</tr>";
    });

    htmlTable += "</tbody>";
    htmlTable += "</table>";

    if ($(".qbDisplayArea").length) {
        $(".qbDisplayArea").html(htmlTable);
    } else {
        if ($("#quantity-breaks-table-grid").length) {
            $("#quantity-breaks-table-grid").remove();
        }
        $(".product-single form").append(htmlTable);
    }
}

function qbShowSelectOption(data) {
    var htmlSelects = "";

    data = data.sort(function (a, b) {
        return b.qty - a.qty;
    });

    for (var i = 1; i <= 15; i++) {
        var item = data.find(function (e) {
            return e.qty <= i;
        });

        var txt = item ? " (" + item.percent + ")" : '';
        htmlSelects += "<option value='" + i + "'>" + i + txt + "</option>";
    }

    $(".js-quantity-selector").html(htmlSelects);
}

function qbMakeRequest(method, endpoint, data, callback) {
    var xmlhttp = new XMLHttpRequest();
    if (qbDetectHttps(endpoint) && !qbDetectServerURI(endpoint)) {
        xmlhttp.withCredentials = true;
    }
    if (method === "GET") {
        if (typeof data === 'function') {
            callback = data;
            data = null;
        } else {
            endpoint += '?' + qbQueryString(data);
            data = null;
        }
    }
    xmlhttp.open(method, endpoint, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status === 200) {
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.send(JSON.stringify(data));
}

function qbMixPrice(price) {
    var floatPart = Number(price).toFixed(2);
    var lastDigit = floatPart.toString().slice(-1);
    var newDigit = lastDigit < 5 ? 0 : 5;
    var replaceDigit = String(floatPart).replace(/\d{1}$/, newDigit);
    return Number(replaceDigit).toFixed(2);
}

function qbQueryString(obj) {
    var str = [];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    }
    return str.join("&");
}

function qbDetectHttps(url) {
    if (url.indexOf("https://") == 0) {
        return true;
    }
    return false;
}

function qbDetectServerURI(url) {
    if (url.indexOf(qbServerUrl) == 0) {
        return true;
    }
    return false;
}

function qbGetTemplate() {
    var classTemplate = $('body').attr('class');
    classTemplate = classTemplate.split('-');
    return classTemplate[1];
}

function qbsetCookie(cname, cvalue, exdays) {
    var expires = "expires=" + exdays;
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function qbgetCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function qbButtonStatus(disabled = true) {
    var checkout_selectors = ["input[name='checkout']", "button[name='checkout']", "[href$='checkout']", "input[name='goto_pp']", "button[name='goto_pp']", "input[name='goto_gc']", "button[name='goto_gc']", ".additional-checkout-button", ".google-wallet-button-holder", ".amazon-payments-pay-button"];
    checkout_selectors.forEach(function (selector) {
        var els = document.querySelectorAll(selector);
        if (typeof els == "object" && els) {
            for (var i = 0; i < els.length; i++) {
                var el = els[i];
                if (typeof el.addEventListener != "function") {
                    return;
                }
                if (disabled) {
                    el.setAttribute("disabled", "");
                } else {
                    el.removeAttribute("disabled", "");
                }

            }
        }
    });
}