'use strict';

const express = require('express');
const router = express.Router();
const Joi = require('joi');
const Regex = require('../helpers/regex');
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const authController = require('../controllers/auth');

router.post('/register',
    validate({
        body: {
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            email: Joi.string().required().regex(Regex.email, 'Email'),
            password: Joi.string().required(),
        }
    }),
    authController.register
);

router.post('/login',
    authController.login
);

router.get('/logout',
    auth.required,
    authController.logout
);

router.get('/me',
    auth.required,
    authController.me
);

module.exports = router;