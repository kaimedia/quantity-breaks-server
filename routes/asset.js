'use strict';

const _ = require('lodash');
const express = require('express');
const router = express.Router();
const config = require('../config');
const Shop = require('../models/shop');
const Group = require('../models/group');
const assetService = require('../services/asset');

router.get('/js/quantity_breaks.js', (req, res, next) => {
    const shopDomain = req.query.shop;
    const styleURI = req.query.styleURI;
    const groupData = `${config.S3_URI}/groups/${shopDomain}.json`;

    if (!shopDomain) {
        return next({});
    }

    res.set('Content-Type', 'application/javascript');
    res.render('quantity_breaks', { apiURI: config.REG_URI, shopDomain: shopDomain, styleURI: styleURI, groupData: groupData });
});

router.post('/generate_variant/:shop', (req, res, next) => {
    let cartItems = req.body.products;
    const shopDomain = req.params.shop;

    if (!shopDomain || !cartItems) {
        return next({});
    }

    assetService.generateVariant(shopDomain, cartItems).then(response => {
        let _oriVariants = {};
        let _oldVariants = {};
        let _newVariants = {};
        let _cartVariant = {};
        let _linePrices = [];
        let reloadPage = false;

        let variantString = response.groupVariantStr;
        let newVariants = response.newVariants;

        if (allIsFalse(newVariants)) {
            return res.status(200).send({});
        }

        cartItems.forEach((e) => {
            if (e) {
                let quantity = e.line_quantity;
                _oriVariants[e.variant_id] = quantity;
                if (variantString.indexOf(e.variant_id) > -1) {
                    quantity = 0;
                }
                _oldVariants[e.variant_id] = quantity;
            }
        });

        newVariants.forEach((e) => {
            if (e) {
                _newVariants[e.custom_variant_id] = e.new_qty;
                _linePrices.push({
                    variant_id: e.custom_variant_id,
                    price: (e.price * e.new_qty).toFixed(2),
                    origin_price: (e.origin_price * e.new_qty).toFixed(2),
                });
                if (_oldVariants[e.origin_variant_id]) {
                    _oldVariants[e.origin_variant_id] = 0;
                }
            }
        });

        _cartVariant = _.defaults(_newVariants, _oldVariants);

        if (!_.isEqual(_cartVariant, _oriVariants)) {
            reloadPage = true;
        }

        res.status(200).send({
            cartUpdate: _cartVariant,
            customVariants: _newVariants,
            reloadPage: reloadPage,
            linePrices: _linePrices,
        });
    }, err => next(err));
});

function allIsFalse(obj) {
    let length = obj.length;
    for (let i = 0; i < length; i++) {
        if (obj[i]) return false;
    }
    return true;
}

module.exports = router;