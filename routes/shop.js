'use strict';

const express = require('express');
const router = express.Router();
const Shop = require('../models/shop');
const auth = require('../middlewares/authentication');

router.get('/list',
    (req, res, next) => {
        Shop.find({ isActive: true }, { shopify_domain: 1, hmac: 1 }).exec().then((responses) => {
            res.status(200).send(responses);
        });
    }
);

module.exports = router;