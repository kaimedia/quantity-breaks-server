'use strict';

const _ = require('lodash');
const express = require('express');
const router = express.Router();
const config = require('../config');
const Shopify = require('shopify-node-api');
const openSession = require('../helpers/shopify').openSession;
const Shop = require('../models/shop');
const Group = require('../models/group');
const batcheCtrl = require('../controllers/batche');
const batcheService = require('../services/batche');

router.post('/uninstall', (req, res, next) => {
    const shopDomain = req.body.myshopify_domain;
    Shop.findOne({ shopify_domain: shopDomain }).exec().then((shop) => {
        const shopAPI = openSession(shop);
        Shop.findOneAndRemove({ shopify_domain: shopDomain }).exec().then(() => {
            batcheCtrl.saveJsonData(shopDomain, true);
            shopAPI.get('/admin/themes.json', (err, body) => {
                if (err) throw err;
                if (Object.keys(body).length) {
                    const currentTheme = _.find(body.themes, el => el.role === 'main');
                    shopAPI.get(`/admin/themes/${currentTheme.id}/assets.json?asset[key]=sections/footer.liquid`, (err, body) => {
                        if (err) throw err;
                        if (body.asset) {
                            let newData = body.asset.value;
                            newData = newData.replace(/<script rel='quantity-breaks-script'.*?>.*?<\/script>/ims, '');
                            shopAPI.put(`/admin/themes/${currentTheme.id}/assets.json`, {
                                asset: {
                                    key: 'sections/footer.liquid',
                                    value: newData,
                                }
                            }, (err, body) => {
                                if (err) throw err;
                                res.end();
                            });
                        }
                    });
                }
            });
        });
    });
});

router.post('/productUpdate', (req, res, next) => {
    const product = req.body;
    const shop = req.query.shop;
    Group.findOne({ shop: shop, productIds: { $regex: String(product.id), $options: "i" }, status: 1 }).exec((err, group) => {
        if (err) {
            if (err) throw err;
            return res.end();
        }
        if (!group) {
            return res.end();
        }
        batcheService.updateVariants(shop, group.products, group.price_levels).then(data => {
            batcheService.saveCustomVariants(group, data).then(() => {
                batcheCtrl.saveJsonData(shop);
                res.status(200).send('Success.');
            }, err => next(err));
        }, err => next(err));
    });
});

module.exports = router;