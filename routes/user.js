'use strict';

const express = require('express');
const router = express.Router();
const Joi = require('joi');
const Regex = require('../helpers/regex');
const validate = require('express-validation');
const acl = require('../middlewares/acl');
const auth = require('../middlewares/authentication');
const userController = require('../controllers/user');

router.use([auth.required, acl.middleware(0, (req) => req.query.uid)]);

router.post('/',
    validate({
        body: {
            firstName: Joi.string().required(),
            lastName: Joi.string().required(),
            email: Joi.string().required().regex(Regex.email, 'Email'),
            password: Joi.string().required(),
        }
    }),
    userController.create
);

router.get('/',
    userController.all
);

router.get('/:id',
    validate({
        params: {
            id: Joi.string().required(),
        }
    }),
    userController.get
);

router.delete('/:id',
    validate({
        params: {
            id: Joi.string().required(),
        }
    }),
    userController.remove
);

router.patch('/',
    validate({
        body: {
            _id: Joi.string().required(),
        }
    }),
    userController.update
);

router.patch('/password',
    validate({
        body: {
            _id: Joi.string().required(),
            newPassword: Joi.string().required(),
        }
    }),
    userController.password
);

module.exports = router;