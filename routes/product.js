'use strict';

const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const productCtrl = require('../controllers/product');

router.use(auth.required);

router.get('/:shop/list',
    validate({
        params: {
            shop: Joi.string().required(),
        }
    }),
    productCtrl.list
);

module.exports = router;