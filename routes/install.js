'use strict';

const _ = require('lodash');
const fs = require('fs');
const AWS = require('aws-sdk');
const request = require('request');
const express = require('express');
const router = express.Router();
const Promise = require('bluebird');
const config = require('../config');
const Shop = require('../models/shop');
const Group = require('../models/group');
const Shopify = require('shopify-node-api');
const BatcheCtrl = require('../controllers/batche');
const generateNonce = require('../helpers/shopify').generateNonce;
const buildWebhook = require('../helpers/shopify').buildWebhook;
const moment = require('moment-timezone');

function __buildWebhook(event, destPath, shopAPI) {
    return new Promise((resolve) => {
        buildWebhook(event, destPath, shopAPI, () => {
            return resolve();
        });
    });
}

router.get('/', (req, res, next) => {
    const shopName = req.query.shop;
    const nonce = generateNonce();
    const query = Shop.findOne({ shopify_domain: shopName }).exec();
    const shopAPI = new Shopify({
        shop: shopName,
        shopify_api_key: config.SHOPIFY_API_KEY,
        shopify_shared_secret: config.SHOPIFY_SHARED_SECRET,
        shopify_scope: config.APP_SCOPE,
        nonce,
        redirect_uri: `${config.REG_URI}/install/callback`,
    });
    const redirectURI = shopAPI.buildAuthURL();
    query.then((response) => {
        let save;
        const shop = response;
        if (!shop) {
            save = new Shop({ shopify_domain: shopName, nonce }).save();
        } else {
            shop.shopify_domain = shopName;
            shop.nonce = nonce;
            save = shop.save();
        }
        return save.then(() => res.redirect(redirectURI));
    });
});

router.get('/callback', (req, res, next) => {
    const params = req.query;
    const query = Shop.findOne({ shopify_domain: params.shop }).exec();
    query.then((result) => {
        const shop = result;
        const shopAPI = new Shopify({
            shop: params.shop,
            shopify_api_key: config.SHOPIFY_API_KEY,
            shopify_shared_secret: config.SHOPIFY_SHARED_SECRET,
            nonce: shop.nonce,
        });
        shopAPI.exchange_temporary_token(params, (error, data) => {
            if (error) {
                next(error);
            }
            shop.hmac = params.hmac;
            shop.accessToken = data.access_token;
            shop.isActive = true;
            shop.save((err) => {
                if (err) {
                    next(err);
                }

                const addressPath = `${config.REG_URI}/webhook`;
                const webhooks = [
                    {
                        "event": "app/uninstalled",
                        "path": "uninstall"
                    },
                    // {
                    //     "event": "products/update",
                    //     "path": "productUpdate"
                    // },
                ];
                let promises = [];
                webhooks.forEach(wh => {
                    promises.push(__buildWebhook(wh.event, `${addressPath}/${wh.path}?shop=${shop.shopify_domain}`, shopAPI));
                });
                Promise.all(promises).then(() => {
                    buildAssets(params.shop).then(scriptURI => {
                        shopAPI.get('/admin/themes.json', (err, body) => {
                            if (err) throw err;
                            if (Object.keys(body).length) {
                                const currentTheme = _.find(body.themes, el => el.role === 'main');
                                shopAPI.get(`/admin/themes/${currentTheme.id}/assets.json?asset[key]=sections/footer.liquid`, (err, body) => {
                                    if (err) throw err;
                                    if (body.asset) {
                                        let newData = body.asset.value;
                                        newData = newData.replace(/<script rel='quantity-breaks-script'.*?>.*?<\/script>/ims, '');
                                        newData = `${newData} \n <script rel='quantity-breaks-script'> \n var _quantity_breaks_script = document.createElement('script'); \n _quantity_breaks_script.setAttribute('src','${scriptURI}?v=' + (new Date().getTime())); \n document.head.appendChild(_quantity_breaks_script); \n </script> \n`;
                                        shopAPI.put(`/admin/themes/${currentTheme.id}/assets.json`, {
                                            asset: {
                                                key: 'sections/footer.liquid',
                                                value: newData,
                                            }
                                        }, (err, body) => {
                                            if (err) throw err;
                                            BatcheCtrl.saveJsonData(shop.shopify_domain);
                                            res.redirect(`https://${shop.shopify_domain}/admin/apps`);
                                        });
                                    }
                                });
                            }
                        });
                    }, err => next(err));
                });
            });
        });
    });
});

function buildAssets(shop) {
    return new Promise((resolve, reject) => {
        const S3 = new AWS.S3({
            accessKeyId: config.AWS_ACCESS_KEY,
            secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
        });
        const timestamp = moment().unix();
        const bucketName = config.S3_BUCKET;
        const bucketFolder = config.S3_FOLDER;
        const bucketUrl = config.S3_URI;
        const styleURI = `${bucketUrl}/${timestamp}.css`;
        const scriptURI = `${bucketUrl}/${timestamp}.js`;
        request({
            url: `${config.REG_URI}/assets/js/quantity_breaks.js`,
            method: 'GET',
            headers: {},
            qs: {
                shop: shop,
                styleURI: styleURI,
            }
        }, (error, response, js) => {
            if (error) throw error;
            fs.readFile('public/css/quantity_breaks.css', (err, css) => {
                if (err) throw err;
                S3.upload({
                    Bucket: bucketName,
                    Key: `${bucketFolder}/${timestamp}.css`,
                    Body: new Buffer(css, 'binary'),
                    ContentType: 'text/css',
                    ACL: 'public-read',
                }, (err, data) => {
                    if (err) throw err;
                });
                S3.upload({
                    Bucket: bucketName,
                    Key: `${bucketFolder}/${timestamp}.js`,
                    Body: new Buffer(js, 'binary'),
                    ContentType: 'application/javascript',
                    ACL: 'public-read',
                }, (err, data) => {
                    if (err) throw err;
                    resolve(scriptURI);
                });
            });
        });
    });
}

module.exports = router;