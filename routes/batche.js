'use strict';

const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validate = require('express-validation');
const auth = require('../middlewares/authentication');
const batcheCtrl = require('../controllers/batche');

router.use(auth.required);

router.get('/:shop/list',
    validate({
        params: {
            shop: Joi.string().required(),
        }
    }),
    batcheCtrl.list
);

router.get('/:shop/:id',
    validate({
        params: {
            id: Joi.string().required(),
            shop: Joi.string().required(),
        }
    }),
    batcheCtrl.get
);

router.post('/',
    validate({
        body: {
            name: Joi.string().required(),
        }
    }),
    batcheCtrl.create
);

router.patch('/:shop/:id',
    validate({
        params: {
            id: Joi.string().required(),
            shop: Joi.string().required(),
        },
        body: {
            name: Joi.string().required(),
        }
    }),
    batcheCtrl.update
);

router.patch('/:shop',
    validate({
        params: {
            shop: Joi.string().required(),
        },
        body: {
            price_levels: Joi.array().required(),
        }
    }),
    batcheCtrl.updateAll
);

router.delete('/:shop/:id',
    validate({
        params: {
            id: Joi.string().required(),
            shop: Joi.string().required(),
        }
    }),
    batcheCtrl.remove
);

module.exports = router;