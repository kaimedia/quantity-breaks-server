'use strict';

const express = require('express');
const router = express.Router();
const verifyOAuth = require('../helpers/shopify').verifyOAuth;
const config = require('../config');
const Shop = require('../models/shop');

router.get('/', (req, res, next) => {
    const query = Object.keys(req.query).map((key) => `${key}=${req.query[key]}`).join('&');
    if (req.query.shop) {
        Shop.findOne({ shopify_domain: req.query.shop, isActive: true }, (err, shop) => {
            if (!shop) {
                return res.redirect(`/install/?${query}`);
            }
            if (req.query.hmac && req.query.hmac === shop.hmac) {
                return res.redirect(`${config.APP_URI}?${query}`);
            }
            if (verifyOAuth(req.query)) {
                return res.redirect(`${config.APP_URI}?${query}`);
            }
            return res.end();
        });
    } else {
        return res.end();
    }
});

module.exports = router;