const env = process.env.NODE_ENV;
const production = require('./production');
const development = require('./development');

let conf = null;
if (env !== 'production') {
    conf = require('./config.json');
}

const config = {
    APP_NAME: 'Quantity Breaks',
    APP_SCOPE: process.env.APP_SCOPE || 'read_products,write_products,read_themes,write_themes,read_script_tags,write_script_tags,read_price_rules,write_price_rules',
    SHOPIFY_API_KEY: process.env.SHOPIFY_API_KEY || conf.SHOPIFY_API_KEY || '',
    SHOPIFY_SHARED_SECRET: process.env.SHOPIFY_SHARED_SECRET || conf.SHOPIFY_SHARED_SECRET || '',
    AWS_ACCESS_KEY: process.env.AWS_ACCESS_KEY || conf.AWS_ACCESS_KEY || '',
    AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY || conf.AWS_SECRET_ACCESS_KEY || '',
    MONGODB_URI: process.env.MONGODB_URI || conf.MONGODB_URI || '',
    SECRET: process.env.SECRET || 'secret',
    DATE_FORMAT: 'YYYY-MM-DD',
    TIMEZONE: 'America/New_York',
    S3_BUCKET: 'ecdn.pasger.com',
    S3_FOLDER: 'quantitybreaks',
    S3_URI: 'https://ecdn.pasger.com/quantitybreaks',
    SENDGRID: {
        apiKey: "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",
        from: "we@magixshop.com"
    }
};

if (env !== 'production') {
    module.exports = Object.assign({}, config, development);
} else {
    module.exports = Object.assign({}, config, production);
}
