'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Group = require('../models/group');
const Shopify = require('shopify-node-api');

function updateVariants(shopDomain, productList, priceLevels) {
    return new Promise((resolve, reject) => {
        let promises = [];
        getShopifyAPI(shopDomain).then((shopAPI) => {
            productList.forEach(product => {
                promises.push(updateVariantsOnProduct(shopAPI, product, priceLevels));
            });
            Promise.all(promises).then(results => {
                resolve(results);
            }).catch(err => reject(err));
        }, err => reject(err));
    });
}

function updateVariantsOnProduct(shopAPI, product, priceLevels) {
    return new Promise((resolve, reject) => {
        let promises = [];
        const productId = product.id;
        getVariants(shopAPI, productId).then((variantList) => {
            if (_.isEmpty(variantList)) {
                return;
            }
            priceLevels.forEach(level => {
                promises.push(saveVariantsLevel(shopAPI, level, variantList, product));
            });
            Promise.all(promises).then(results => {
                let customVariants = [];
                results.forEach(element => {
                    element.variantIds.forEach(variant => {
                        customVariants.push({
                            origin_variant_id: variant.origin_variant_id,
                            custom_variant_id: variant.custom_variant_id,
                            qty: variant.qty,
                            price: variant.price,
                            origin_price: variant.origin_price,
                        });
                    });
                });
                resolve({ productId: productId, customVariants: customVariants, variants: searchVariants(variantList).originVariants });
            }).catch(err => reject(err));
        }, err => reject(err));
    });
}

function getVariants(shopAPI, productId) {
    return new Promise((resolve, reject) => {
        shopAPI.get(`/admin/products/${productId}/variants.json`, (err, res) => {
            if (err) return reject(err);
            resolve(res.variants);
        });
    });
}

function saveVariantsLevel(shopAPI, priceLevel, variantList, productItem) {
    return new Promise((resolve, reject) => {
        let promises = [], time_out = 0;
        const originVariants = searchVariants(variantList).originVariants;

        originVariants.forEach(variant => {
            promises.push(saveVariant(shopAPI, priceLevel, productItem, variantList, variant, time_out));
            time_out += 200;
        });

        Promise.all(promises).then(results => {
            changeProductTemplate(shopAPI, productItem.id).then(() => {
                resolve({ variantIds: results });
            }, err => reject(err));
        }).catch(err => reject(err));
    });
}

function checkVariantExisted(variantList, customTitle) {
    return new Promise((resolve, reject) => {
        const variant = variantList.find(function (e) { return e.title.indexOf(customTitle) != -1 });
        if (variant) {
            return resolve(variant);
        }
        resolve(null);
    });
}

function saveVariant(shopAPI, level, product, variants, variant, time_out) {
    return new Promise((resolve, reject) => {
        let optionArr = [];
        let percent = `/ ${level.value}% OFF`;
        let option1 = variant.option1 ? `${variant.option1} ${level.qty}+ ${percent}` : variant.option1;
        let option2 = variant.option2 ? `${variant.option2} ${level.qty}+ ${percent}` : variant.option2;
        let option3 = variant.option3 ? `${variant.option3} ${level.qty}+ ${percent}` : variant.option3;
        option1 = removeDefaultTitle(option1);
        option2 = removeDefaultTitle(option2);
        option3 = removeDefaultTitle(option3);
        if (option1) optionArr.push(option1);
        if (option2) optionArr.push(option2);
        if (option3) optionArr.push(option3);
        const customTitle = optionArr.join(' / ');
        const price = mixPrice(_.round(parseFloat(variant.price) - ((parseFloat(variant.price) * parseFloat(level.value)) / 100), 2).toFixed(2));
        const position = 50 + Math.floor(Math.random() * 120);

        let dataVariant = {
            variant: {
                option1: option1,
                option2: option2,
                option3: option3,
                price: price,
                sku: variant.sku,
                position: position,
                inventory_policy: variant.inventory_policy,
                compare_at_price: variant.compare_at_price,
                fulfillment_service: variant.fulfillment_service,
                inventory_management: variant.inventory_management,
                taxable: variant.taxable,
                barcode: variant.barcode,
                grams: variant.grams,
                image_id: variant.image_id,
                inventory_quantity: variant.inventory_quantity,
                weight: variant.weight,
                weight_unit: variant.weight_unit,
                old_inventory_quantity: variant.old_inventory_quantity,
                requires_shipping: variant.requires_shipping,
            }
        };

        checkVariantExisted(variants, customTitle).then(checkVariant => {
            setTimeout(() => {
                if (checkVariant) {
                    dataVariant.variant.id = checkVariant.id;
                    shopAPI.put(`/admin/variants/${checkVariant.id}.json`, dataVariant, (err, res) => {
                        if (err) return logger.error(err);
                        const variantId = res.variant.id;
                        resolve({
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            origin_price: parseFloat(variant.price),
                            price: parseFloat(price),
                            qty: level.qty,
                        });
                    });
                } else {
                    shopAPI.post(`/admin/products/${product.id}/variants.json`, dataVariant, (err, res) => {
                        if (err) return logger.error(err);
                        const variantId = res.variant.id;
                        shopAPI.post(`/admin/products/${product.id}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_quantity_breaks', value_type: 'string', key: 'hide_variant', value: '1' } }, (err, body) => { });
                        resolve({
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            origin_price: parseFloat(variant.price),
                            price: parseFloat(price),
                            qty: level.qty,
                        });
                    });
                }
            });
        });
    });
}

function removeDefaultTitle(title) {
    if (title && title.indexOf("Default") != -1) {
        title = title.replace("Default Title", "");
    }
    return title;
}

function changeProductTemplate(shopAPI, productId, empty = false) {
    return new Promise((resolve, reject) => {
        shopAPI.put(`/admin/products/${productId}.json`, {
            product: {
                id: productId,
                template_suffix: !empty ? 'break' : '',
            }
        }, (err, res) => {
            if (err) throw err;
            resolve(res);
        });
    });
}

function mixPrice(price) {
    const floatPart = Number(price).toFixed(2);
    const lastDigit = floatPart.toString().slice(-1);
    const newDigit = lastDigit < 5 ? 0 : 5;
    const replaceDigit = String(floatPart).replace(/\d{1}$/, newDigit);
    return Number(replaceDigit).toFixed(2);
}

function searchVariants(variantList) {
    let originVariants = [];
    let customVariants = [];
    variantList.forEach(e => {
        let pattOrigin = new RegExp(/[!@#$%^&*()_+=\[\]{};':"\\|,.<>?]/g);
        let pattCustom = new RegExp(/(\d+)\+ \/\ (\d+)\% OFF/g);
        if (!pattOrigin.test(e.title)) {
            originVariants.push(e);
        }
        if (pattCustom.test(e.title)) {
            customVariants.push(e);
        }
    });
    return {
        originVariants,
        customVariants,
    };
}

function removeVariants(shopDomain, productList) {
    return new Promise((resolve, reject) => {
        let promises = [];
        getShopifyAPI(shopDomain).then((shopAPI) => {
            productList.forEach(product => {
                promises.push(removeVariantOnProduct(shopAPI, product));
            });
        }, err => reject(err));
        Promise.all(promises).then(res => {
            changeProductTemplate(shopAPI, product.id, true).then(() => {
                resolve(res);
            }, err => reject(err));
        }).catch(err => reject(err));
    });
}

function removeVariantOnProduct(shopAPI, productItem) {
    return new Promise((resolve, reject) => {
        const productId = productItem.id;
        getVariants(shopAPI, productId).then((variantList) => {
            if (_.isEmpty(variantList)) {
                return;
            }
            removeCustomVariants(shopAPI, productId, variantList).then((res) => {
                resolve(res);
            }, err => reject(err));
        }, err => reject(err));
    });
}

function removeCustomVariants(shopAPI, productId, variantList) {
    return new Promise((resolve, reject) => {
        let promises = [];
        let customVariants = searchVariants(variantList).customVariants;
        if (!customVariants.length) {
            return resolve([]);
        }
        customVariants.forEach(variant => {
            promises.push(removeVariant(shopAPI, productId, variant.id));
        });
        Promise.all(promises).then(res => {
            resolve(res);
        }).catch(err => reject(err));
    });
}

function removeVariant(shopAPI, productId, variantId) {
    return new Promise((resolve, reject) => {
        shopAPI.delete(`/admin/products/${productId}/variants/${variantId}.json`, (err, res) => {
            if (err) throw err;
            resolve(res);
        });
    });
}

function getShopifyAPI(shopDomain) {
    return new Promise((resolve, reject) => {
        Shop.findOne({ shopify_domain: shopDomain }).exec((err, shop) => {
            if (err) throw err;
            const shopAPI = new Shopify({
                shop: shopDomain,
                shopify_api_key: config.SHOPIFY_API_KEY,
                shopify_shared_secret: config.SHOPIFY_SHARED_SECRET,
                access_token: shop.accessToken,
            });
            resolve(shopAPI);
        });
    });
}

function saveCustomVariants(group, variants) {
    return new Promise((resolve, reject) => {
        const groupId = group.id;
        const productList = group.products;
        _.map(productList, product => {
            const productInfo = _.find(variants, ['productId', product.id]);
            product.qbVariants = productInfo.customVariants;
            product.variants = searchVariants(productInfo.variants).originVariants;
            return product;
        });
        const query = Group.findByIdAndUpdate(groupId, { products: productList }).exec();
        query.then((response) => {
            resolve(response);
        });
    });
}

module.exports = { updateVariants, removeVariants, saveCustomVariants, getVariants, getShopifyAPI, searchVariants, mixPrice, removeDefaultTitle, checkVariantExisted }