'use strict';

const _ = require('lodash');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Group = require('../models/group');
const Shopify = require('shopify-node-api');
const batche = require('./batche');
const batcheCtrl = require('../controllers/batche');

function run(shop, groups, levels) {
    return new Promise((resolve, reject) => {
        batche.getShopifyAPI(shop).then(API => {
            groups.map((group) => () => processGroup(shop, API, group, levels)).reduce(
                (p, next) => p.then(next),
                Promise.resolve()
            ).then((res) => {
                resolve(res);
            });
        });
    });
}

function processGroup(shop, API, group, levels) {
    let funcs = [onGroup];
    return funcs.reduce((current, next) => {
        return current.then(() => {
            return next(API, group, levels);
        }).then(res => {
            //group done
            let saveJson = setTimeout(() => {
                batcheCtrl.saveJsonData(shop).then(() => clearTimeout(saveJson));
            }, 120000);
        });
    }, Promise.resolve());
}

function onGroup(API, group, levels) {
    return new Promise((resolve, reject) => {
        Group.findByIdAndUpdate(group.id, { price_levels: levels, updated_at: new Date() }).exec(() => {
            updateProduct(API, group, levels).then(res => { resolve(res) });
        });
    });
}

function updateProduct(API, group, levels) {
    return new Promise((resolve, reject) => {
        group.products.map((product) => () => processProduct(API, group, product, levels)).reduce(
            (p, next) => p.then(next),
            Promise.resolve()
        ).then((res) => {
            resolve(res);
        });
    });
}

function processProduct(API, group, product, levels) {
    let funcs = [onProduct];
    return funcs.reduce((current, next) => {
        return current.then(() => {
            return next(API, group, product, levels);
        }).then(res => {
            //product done
        });
    }, Promise.resolve());
}

function onProduct(API, group, product, levels) {
    return new Promise((resolve, reject) => {
        batche.getVariants(API, product.id).then(variants => {
            const products = group.products.map((p) => {
                if (p.id == product.id) p.variants = batche.searchVariants(variants).originVariants;
                return p;
            });
            Group.findByIdAndUpdate(group.id, { products: products }).exec(() => {
                updateLevel(API, group, product, variants, levels).then(res => resolve(res));
            });
        });
    });
}

function updateLevel(API, group, product, variants, levels) {
    return new Promise((resolve, reject) => {
        levels.map((level) => () => processLevel(API, group, product, variants, level)).reduce(
            (p, next) => p.then(next),
            Promise.resolve()
        ).then((res) => {
            resolve(res);
        });
    });
}

function processLevel(API, group, product, variants, level) {
    let funcs = [onLevel];
    return funcs.reduce((current, next) => {
        return current.then(() => {
            return next(API, group, product, variants, level);
        }).then(res => {
            //level done
        });
    }, Promise.resolve());
}

function onLevel(API, group, product, variants, level) {
    return new Promise((resolve, reject) => {
        const origin_variants = batche.searchVariants(variants).originVariants;
        updateVariant(API, group, product, variants, level, origin_variants).then(res => resolve(res));
    });
}

function updateVariant(API, group, product, variants, level, origin_variants) {
    return new Promise((resolve, reject) => {
        origin_variants.map((variant) => () => processVariant(API, group, product, variants, level, variant)).reduce(
            (p, next) => p.then(next),
            Promise.resolve()
        ).then((res) => {
            resolve(res);
        });
    });
}

function processVariant(API, group, product, variants, level, variant) {
    let funcs = [onVariant];
    return funcs.reduce((current, next) => {
        return current.then(() => {
            return next(API, group, product, variants, level, variant);
        }).then(res => {
            logger.info(`${product.title} - ${res.title} was updated successfully!`);
        });
    }, Promise.resolve());
}

function onVariant(API, group, product, variants, level, variant) {
    return new Promise((resolve, reject) => {
        let optionArray = [];
        let percent = `/ ${level.value}% OFF`;
        let price = batche.mixPrice(_.round(parseFloat(variant.price) - ((parseFloat(variant.price) * parseFloat(level.value)) / 100), 2).toFixed(2));
        let position = 50 + Math.floor(Math.random() * 150);
        let option1 = variant.option1 ? `${variant.option1} ${level.qty}+ ${percent}` : variant.option1;
        let option2 = variant.option2 ? `${variant.option2} ${level.qty}+ ${percent}` : variant.option2;
        let option3 = variant.option3 ? `${variant.option3} ${level.qty}+ ${percent}` : variant.option3;
        option1 = batche.removeDefaultTitle(option1);
        option2 = batche.removeDefaultTitle(option2);
        option3 = batche.removeDefaultTitle(option3);
        if (option1) optionArray.push(option1);
        if (option2) optionArray.push(option2);
        if (option3) optionArray.push(option3);
        let customTitle = optionArray.join(' / ');

        let data = {
            variant: {
                price: price,
                sku: variant.sku,
                option1: option1,
                option2: option2,
                option3: option3,
                position: position,
                grams: variant.grams,
                weight: variant.weight,
                taxable: variant.taxable,
                barcode: variant.barcode,
                image_id: variant.image_id,
                weight_unit: variant.weight_unit,
                inventory_policy: variant.inventory_policy,
                compare_at_price: variant.compare_at_price,
                requires_shipping: variant.requires_shipping,
                inventory_quantity: variant.inventory_quantity,
                fulfillment_service: variant.fulfillment_service,
                inventory_management: variant.inventory_management,
                old_inventory_quantity: variant.old_inventory_quantity,
            }
        };

        batche.checkVariantExisted(variants, customTitle).then((checkVariant) => {
            setTimeout(() => {
                if (checkVariant) {
                    data.variant.id = checkVariant.id;
                    API.put(`/admin/variants/${checkVariant.id}.json`, data, (err, res) => {
                        if (err) return resolve(err);
                        const data = {
                            title: res.variant.title,
                            origin_variant_id: variant.id,
                            custom_variant_id: res.variant.id,
                            origin_price: parseFloat(variant.price),
                            price: parseFloat(price),
                            percent: level.value,
                            qty: level.qty,
                        };
                        saveVariant(group, product, data).then((result) => resolve(result));
                    });
                } else {
                    API.post(`/admin/products/${product.id}/variants.json`, data, (err, res) => {
                        if (err) return resolve(err);
                        const variantId = res.variant.id;
                        const variantTitle = res.variant.title;
                        API.post(`/admin/products/${product.id}/variants/${variantId}/metafields.json`, { metafield: { namespace: 'kai_quantity_breaks', value_type: 'string', key: 'hide_variant', value: '1' } }, (err, body) => { });
                        const data = {
                            title: variantTitle,
                            origin_variant_id: variant.id,
                            custom_variant_id: variantId,
                            origin_price: parseFloat(variant.price),
                            price: parseFloat(price),
                            percent: level.value,
                            qty: level.qty,
                        };
                        saveVariant(group, product, data).then((result) => resolve(result));
                    });
                }
            }, 200);
        });
    });
}

function saveVariant(group, product, data) {
    return new Promise((resolve, reject) => {
        const products = group.products.map((p) => {
            p.qbVariants = p.qbVariants || [];
            if (p.id == product.id) {
                const currVariant = _.find(p.qbVariants, ['custom_variant_id', data.custom_variant_id]);
                if (!currVariant) {
                    p.qbVariants.push(data);
                } else {
                    p.qbVariants = _.unionBy(p.qbVariants, [{
                        custom_variant_id: data.custom_variant_id,
                        origin_price: data.origin_price,
                        percent: data.percent,
                        title: data.title,
                        price: data.price,
                        qty: data.qty,
                    }], 'custom_variant_id');
                }
            }
            return p;
        });
        Group.findByIdAndUpdate(group.id, { products: products }).exec((err, result) => resolve(data));
    });
}

module.exports = { run };