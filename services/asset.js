'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const config = require('../config');
const logger = require('../lib/logger');
const Shop = require('../models/shop');
const Group = require('../models/group');

function generateVariant(shop, items) {
    return new Promise((resolve, reject) => {
        let promises = [];
        items.forEach(item => {
            promises.push(variantOnProduct(shop, item));
        });
        Promise.all(promises).then(res => {
            let newVariants = [];
            let groupVariantStr = [];
            res.forEach(element => {
                newVariants.push(element.newVariant);
                groupVariantStr.push(element.groupVariantStr);
            });
            resolve({
                newVariants: newVariants,
                groupVariantStr: groupVariantStr.join(","),
            });
        }).catch(err => reject(err));
    });
}

function variantOnProduct(shop, item) {
    return new Promise((resolve, reject) => {
        let isValid = false;
        let isReset = false;
        let resetVariant = null;
        let newVariant = null;
        let currentProduct = null;
        let currentVariants = [];
        let groupVariantStr = [];
        const productID = item.product_id;
        const variantID = item.variant_id;
        const variantQty = item.line_quantity;
        const query = Group.findOne({ shop: shop, productIds: { $regex: String(productID), $options: "i" }, status: 1 }).exec();
        query.then((response) => {
            const group = response;
            if (!group) {
                return resolve(isValid);
            }
            currentProduct = _.find(group.products, ['id', productID]);
            currentVariants = _.filter(currentProduct.qbVariants, ['origin_variant_id', variantID]);
            if (_.isEmpty(currentVariants)) {
                isReset = true;
                const thisVariant = _.find(currentProduct.qbVariants, ['custom_variant_id', variantID]);
                currentVariants = _.filter(currentProduct.qbVariants, ['origin_variant_id', thisVariant.origin_variant_id]);
            }
            currentVariants = _.orderBy(currentVariants, 'qty', 'desc');
            currentVariants.forEach(element => {
                groupVariantStr.push(element.custom_variant_id);
            });
            const countVariant = Object.keys(currentVariants).length;
            for (let i = 0; i < countVariant; i++) {
                if (variantQty >= currentVariants[i].qty) {
                    currentVariants[i].new_qty = variantQty;
                    newVariant = currentVariants[i];
                    break;
                }
                if (isReset && variantQty <= currentVariants[i].qty) {
                    newVariant = {
                        origin_variant_id: currentVariants[i].custom_variant_id,
                        custom_variant_id: currentVariants[i].origin_variant_id,
                        qty: currentVariants[i].qty,
                        new_qty: variantQty,
                    };
                }
            }
            if (!newVariant) {
                return resolve(isValid);
            }
            resolve({
                newVariant,
                groupVariantStr,
            });
        });
    });
}

exports.generateVariant = generateVariant;