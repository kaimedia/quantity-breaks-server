const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var userSchema = new Schema({
    shop: String,
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: String,
    lastName: String,
    address: String,
    productManagement: Object,
    productTypeManagement: Object,
    token: String,
    fbAdsId: String,
    role: {
        type: String,
        default: 'guest'
    },
    active: {
        type: Boolean,
        default: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
    ads: {
        spend: {
            type: Number,
            default: 0
        }
    },
    totalSales: {
        type: Number,
        default: 0
    },
    profit: {
        type: Number,
        default: 0
    },
    kpiLevel: {
        type: String,
        default: 'N/A'
    },
});

userSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

const User = mongoose.model('User', userSchema);
module.exports = User;