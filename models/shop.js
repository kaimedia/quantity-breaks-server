'user strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ShopSchema = mongoose.Schema({
    shopId: Number,
    shopify_domain: {
        type: String,
        unique: true,
        required: true
    },
    name: String,
    domain: String,
    supportEmail: String,
    nonce: String,
    hmac: String,
    accessToken: String,
    isActive: {
        type: Boolean,
        default: false
    },
    scriptId: Number,
});

module.exports = mongoose.model('Shop', ShopSchema);