'user strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var GroupSchema = mongoose.Schema({
    shop: {
        type: String,
        require: true,
    },
    name: {
        type: String,
        require: true,
    },
    price_type: {
        type: String,
        require: true,
        default: '%',
    },
    across_variants: {
        type: String,
        require: true,
        default: 'variant',
    },
    price_levels: {
        type: Object,
        require: true,
    },
    products: Object,
    productIds: String,
    settings: {
        type: Object,
        default: {
            showTable: true,
        }
    },
    status: {
        type: Number,
        default: 0,
    },
    created_at: {
        type: Date,
        default: Date.now,
    },
    updated_at: {
        type: Date,
        default: Date.now
    },
});

GroupSchema.pre('save', function (next) {
    if (!this.updated_at)
        this.updated_at = Date.now();
    next();
});

module.exports = mongoose.model('group', GroupSchema);